import React from 'react'


const SearchBar = (props) => {
    return(
        <div style={styles.contentSearch}>
            <input 
                style={styles.inputSearch}
                type="text" 
                placeholder="search here..." 
                onChange={props.onChangeSearch}
            />
        </div>
    )
}

const styles = {
    inputSearch:{
        width: 300,
        height: 30,
    },
    contentSearch:{
        backgroundColor: "blue",
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        display: "flex"
    }
}
    


export default SearchBar