import React from "react";

const BlogList = props => {
  return (
    <div style={styles.bg}>
      <h2>{props.title}</h2>
      <p>{props.author}</p>
      <p style={styles.isi}>{props.content}</p>
      <p style={styles.test} >{props.created_at}</p>
    </div>
  );
};


const styles = {
    bg:{
        backgroundColor: "#ddd",
        margin: "10px",
        padding: "15px",
    },
    bga:{
        backgroundColor: "#ddd",
    },
    test:{
        color: "blue",
        textAlign: "right",
    },
    isi:{
        padding: 10,
    }
}

export default BlogList;