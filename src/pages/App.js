import React, { Component } from 'react'
import SearchBar from '../components/SearchBar'
import BlogList from '../components/BlogList'

const link =
  "https://cdn.rawgit.com/kevinhermawan/ca5e0083648ba5ffb2421808d972dd9c/raw/c29c7ee02849b58024fb6a058acae33bde38cbd3/react-blog-example.json";


class App extends Component{
    // constructor
    constructor(){
        super()

        this.state = {
            loading:true,
            blogs: [],
            blogFilter: []
            // load: 'lorem'
        }
    }
    // constructor

    componentDidMount(){
        this.handleGetBlogs()
    }

    handleTypeSearch = event => {
        const a = this.state.blogs.filter((blogs) => {
            return blogs.title.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1
        });

        this.setState({ blogFilter: a })
    }

    // componentDidMount(){
    //     setTimeout(() => {
    //     this.setState({loading: false})
    //     }, 1000)
    // }

    handleGetBlogs(){
        fetch(link)
        .then(res => res.json())
        .then(res => this.setState({blogs: res, blogFilter: res}))
    }

    render(){

        console.log(this.state.blogFilter)
        
        // if(this.state.loading){
        //     return(
        //         <h1>Loading</h1>
        //     )
        // }


        return(
            <div>
                <SearchBar 
                search={this.state.search}
                onChangeSearch={this.handleTypeSearch}
            />
            {this.state.blogFilter.map((blogs, index) => (
                <BlogList 
                    key={index}
                    title={blogs.title}
                    content={blogs.content}
                    author={blogs.author}
                    created_at={blogs.created_at}
                />
            ))}

            </div>
        )
    }
}




export default App